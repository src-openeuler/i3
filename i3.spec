Name:           i3
Version:        4.24
Release:        1
Summary:        Improved tiling window manager
License:        BSD-3-Clause
URL:            https://i3wm.org
Source0:        %{URL}/downloads/%{name}-%{version}.tar.xz
Source1:        %{URL}/downloads/%{name}-%{version}.tar.xz.asc
Source2:        gpgkey-424E14D703E7C6D43D9D6F364E7160ED4AC8EE1D.gpg
Source3:        %{name}-logo.svg
 
# i3-gaps was merged into i3 with 4.22
Provides:       i3-gaps = %{version}-%{release}
Obsoletes:      i3-gaps < 4.22-1

BuildRequires:  gcc
# need at least 0.53 to build the documentation
BuildRequires:  meson >= 0.53
# from meson.build
BuildRequires:  pkg-config >= 0.25
# no pkg-config for libev
BuildRequires:  libev-devel >= 4.0
BuildRequires:  pkgconfig(libstartup-notification-1.0)
BuildRequires:  pkgconfig(xcb) >= 1.1.93
BuildRequires:  pkgconfig(xcb-xkb)
BuildRequires:  pkgconfig(xcb-xinerama)
BuildRequires:  pkgconfig(xcb-randr)
BuildRequires:  pkgconfig(xcb-shape)
BuildRequires:  pkgconfig(xcb-event)
BuildRequires:  pkgconfig(xcb-util)
BuildRequires:  pkgconfig(xcb-cursor)
BuildRequires:  pkgconfig(xcb-keysyms)
BuildRequires:  pkgconfig(xcb-icccm)
BuildRequires:  pkgconfig(xcb-xrm)
BuildRequires:  pkgconfig(xkbcommon) >= 0.4.0
BuildRequires:  pkgconfig(xkbcommon-x11) >= 0.4.0
BuildRequires:  pkgconfig(yajl) >= 2.0.1
BuildRequires:  pkgconfig(libpcre) >= 8.10
BuildRequires:  pkgconfig(cairo) >= 1.14.4
BuildRequires:  pkgconfig(pangocairo) >= 1.30.0
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gobject-2.0)
# man pages
BuildRequires:  asciidoc >= 8.3.0
BuildRequires:  xmlto >= 0.0.23
 
# TODO: Testsuites
BuildRequires:  desktop-file-utils
BuildRequires:  perl(AnyEvent)
BuildRequires:  perl(AnyEvent::Util)
BuildRequires:  perl(AnyEvent::Handle)
BuildRequires:  perl(AnyEvent::I3)
BuildRequires:  perl(ExtUtils::MakeMaker)

BuildRequires:  perl-generators
BuildRequires:  perl(Pod::Simple)

# gpg verification
BuildRequires:  gnupg2

Requires:       xorg-x11-fonts-misc
# packages autostarted by the config
Recommends:     dex-autostart
Recommends:     xss-lock
Recommends:     network-manager-applet
Recommends:     pulseaudio-utils
Recommends:     dmenu
 
# for i3-save-tree
Requires:       perl(AnyEvent::I3) >= 0.12
# the config:
Requires:       (i3-config)
 
Recommends:     i3status
Recommends:     i3lock
Recommends:     i3-config
 
%description
Key features of i3 are correct implementation of XrandR, horizontal and vertical
columns (think of a table) in tiling. Also, special focus is on writing clean,
readable and well documented code. i3 uses xcb for asynchronous communication
with X11, and has several measures to be very fast.
 
Please be aware that i3 is primarily targeted at advanced users and developers.
 
%package        config
Summary:        Upstream configuration for %{name}
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
Recommends:     rxvt-unicode
 
%description    config
This is the upstream/vanilla configuration file of i3.
 
%package        doc
Summary:        Documentation for %{name}
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
 
%description    doc
Asciidoc generated documentation for %{name}.
 
%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}
 
%description    devel
Header files for %{name}.
 
%package        devel-doc
Summary:        Documentation for the development files of %{name}
BuildRequires:  doxygen
BuildArch:      noarch
Requires:       %{name} = %{version}-%{release}
 
%description    devel-doc
Doxygen generated documentations for %{name}.
 
%prep
gpg --import '%{S:2}'
gpg --verify '%{S:1}' '%{S:0}'
%autosetup -p1
 
# Drop /usr/bin/env lines in those which will be installed to %%_bindir.
find . -maxdepth 1 -type f -name "i3*" -exec sed -i -e '1s;^#!/usr/bin/env perl;#!/usr/bin/perl;' {} + -print
 
 
%build
%meson
%meson_build
doxygen pseudo-doc.doxygen
mv pseudo-doc/html pseudo-doc/doxygen
 
%install
%meson_install
mkdir -p %{buildroot}%{_mandir}/man1/
install -Dpm0644 man/*.1 \
        %{buildroot}%{_mandir}/man1/
 
mkdir -p %{buildroot}%{_datadir}/pixmaps/
install -Dpm0644 %{SOURCE3} \
        %{buildroot}%{_datadir}/pixmaps/ 
%check
desktop-file-validate %{buildroot}%{_datadir}/applications/i3.desktop
 

%files
%doc RELEASE-NOTES-%{version}
%license LICENSE
%{_bindir}/%{name}*
%dir %{_sysconfdir}/%{name}/
%{_datadir}/xsessions/%{name}.desktop
%{_datadir}/xsessions/%{name}-with-shmlog.desktop
%{_mandir}/man*/%{name}*
%{_datadir}/pixmaps/%{name}-logo.svg
%{_datadir}/applications/%{name}.desktop
%exclude %{_docdir}/%{name}/
 
%files config
%config(noreplace) %{_sysconfdir}/%{name}/config
%config %{_sysconfdir}/%{name}/config.keycodes
 
%files doc
%doc docs/*.{html,png}
 
%files devel
%{_includedir}/%{name}/
 
%files devel-doc
%doc pseudo-doc/doxygen/
 
%changelog
* Fri Jan 17 2025 Funda Wang <fundawang@yeah.net> - 4.24-1
- update to 4.24

* Thu Nov 09 2023 lvgenggeng <lvgenggeng@uniontech.com> 4.23-1
- bump to 4.23

* Wed Nov 08 2023 lvgenggeng <lvgenggeng@uniontech.com> - 4.22-2
- backport patches from upstream

* Fri Feb 10 2023 lichaoran <pkwarcraft@hotmail.com> - 4.22-1
- Initial packaging
